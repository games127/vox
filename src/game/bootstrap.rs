use bevy::{
  math::{Vec2, Vec3},
  pbr::{
    wireframe::Wireframe, AlphaMode, PbrBundle, PointLight, PointLightBundle, StandardMaterial,
  },
  prelude::{
    default, shape, AssetServer, Assets, Color, Commands, Component, Handle, Image, KeyCode, Mesh,
    PerspectiveCameraBundle, Res, ResMut, Transform,
  },
  render::mesh::{Indices, PrimitiveTopology},
};

use super::{components::chunk::Chunk, resources::VisibleChunks};

#[derive(Component)]
pub struct Rotator;

#[derive(Component)]
pub struct CameraController {
  pub enabled: bool,
  pub sensitivity: f32,
  pub key_forward: KeyCode,
  pub key_back: KeyCode,
  pub key_left: KeyCode,
  pub key_right: KeyCode,
  pub key_up: KeyCode,
  pub key_down: KeyCode,
  pub key_run: KeyCode,
  pub walk_speed: f32,
  pub run_speed: f32,
  pub friction: f32,
  pub pitch: f32,
  pub yaw: f32,
  pub velocity: Vec3,
}

impl Default for CameraController {
  fn default() -> Self {
    Self {
      enabled: true,
      sensitivity: 0.5,
      key_forward: KeyCode::W,
      key_back: KeyCode::S,
      key_left: KeyCode::A,
      key_right: KeyCode::D,
      key_up: KeyCode::E,
      key_down: KeyCode::Q,
      key_run: KeyCode::LShift,
      walk_speed: 10.0,
      run_speed: 30.0,
      friction: 0.5,
      pitch: 0.0,
      yaw: 0.0,
      velocity: Vec3::ZERO,
    }
  }
}

pub fn bootstrap(
  mut commands: Commands,
  mut visible_chunks: ResMut<VisibleChunks>,
  mut meshes: ResMut<Assets<Mesh>>,
  mut materials: ResMut<Assets<StandardMaterial>>,
  asset_server: Res<AssetServer>,
) {
  let texture_handle: Handle<Image> = asset_server.load("textures/vox-pallete.png");

  let material_handle = materials.add(StandardMaterial {
    base_color_texture: Some(texture_handle.clone()),
    alpha_mode: AlphaMode::Blend,
    unlit: true,
    ..default()
  });

  visible_chunks.chunks = vec![
    Vec2::new(-1.0, -1.0),
    Vec2::new(0.0, -1.0),
    Vec2::new(1.0, -1.0),
    Vec2::new(-1.0, 0.0),
    Vec2::new(0.0, 0.0),
    Vec2::new(1.0, 0.0),
    Vec2::new(-1.0, 1.0),
    Vec2::new(0.0, 1.0),
    Vec2::new(1.0, 1.0),
  ];

  //   let visible_chunks = VisibleChunks::default();

  //   for [x, y] in visible_chunks.chunks {
  //     let chunk = Chunk::new(Vec2::new(x as f32, y as f32)).generate();
  //     commands
  //       .spawn()
        // .insert_bundle(PbrBundle {
  //         mesh: meshes.add(chunk.generate_mesh()),
  //         material: material_handle.clone(),
  //         ..default()
  //       })
  //       .insert(Wireframe)
  //       .insert(chunk);
  //   }

  //   commands.spawn().insert(visible_chunks);

  //   commands
  //     .spawn()
  //     .insert_bundle(PbrBundle {
  //       mesh: meshes.add(mesh),
  //       material: material_handle,
  //       transform: Transform::from_xyz(-1.5, 0.0, -1.5),
  //       ..default()
  //     })
  //     // .insert(Rotator)
  //     .insert(Wireframe);

  //   commands.spawn_bundle(PointLightBundle {
  //     point_light: PointLight {
  //       intensity: 1500.0,
  //       shadows_enabled: true,
  //       ..default()
  //     },
  //     transform: Transform::from_xyz(4.0, 8.0, 4.0),
  //     ..default()
  //   });
  commands
    .spawn()
    .insert_bundle(PerspectiveCameraBundle {
      transform: Transform::from_xyz(8.0, 8.0, 8.0).looking_at(Vec3::ZERO, Vec3::Y),
      ..default()
    })
    .insert(CameraController::default());
}
