use bevy::math::Vec2;

#[derive(Default, Debug)]
pub struct VisibleChunks {
  pub chunks: Vec<Vec2>,
}
