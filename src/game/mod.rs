use bevy::{
  core::Time,
  input::{mouse::MouseMotion, Input},
  math::{EulerRot, Quat, Vec2, Vec3},
  pbr::{
    wireframe::{Wireframe, WireframePlugin},
    PbrBundle,
  },
  prelude::{
    default, Assets, Camera, Commands, Entity, EventReader, Handle, KeyCode, Mesh, Plugin, Query,
    Res, ResMut, Transform, With, Without, GlobalTransform,
  },
};
use bootstrap::bootstrap;

use self::{
  bootstrap::CameraController,
  components::chunk::{self, Chunk},
  resources::VisibleChunks,
};

mod bootstrap;
mod components;
mod resources;

pub struct VoxPlugin;

fn camera_controller(
  time: Res<Time>,
  mut mouse_events: EventReader<MouseMotion>,
  key_input: Res<Input<KeyCode>>,
  mut query: Query<(&mut Transform, &mut CameraController), With<Camera>>,
) {
  let dt = time.delta_seconds();

  // Handle mouse input
  let mut mouse_delta = Vec2::ZERO;
  for mouse_event in mouse_events.iter() {
    mouse_delta += mouse_event.delta;
  }

  for (mut transform, mut options) in query.iter_mut() {
    if !options.enabled {
      continue;
    }

    // Handle key input
    let mut axis_input = Vec3::ZERO;
    if key_input.pressed(options.key_forward) {
      axis_input.z += 1.0;
    }
    if key_input.pressed(options.key_back) {
      axis_input.z -= 1.0;
    }
    if key_input.pressed(options.key_right) {
      axis_input.x += 1.0;
    }
    if key_input.pressed(options.key_left) {
      axis_input.x -= 1.0;
    }
    if key_input.pressed(options.key_up) {
      axis_input.y += 1.0;
    }
    if key_input.pressed(options.key_down) {
      axis_input.y -= 1.0;
    }

    // Apply movement update
    if axis_input != Vec3::ZERO {
      let max_speed = if key_input.pressed(options.key_run) {
        options.run_speed
      } else {
        options.walk_speed
      };
      options.velocity = axis_input.normalize() * max_speed;
    } else {
      let friction = options.friction.clamp(0.0, 1.0);
      options.velocity *= 1.0 - friction;
      if options.velocity.length_squared() < 1e-6 {
        options.velocity = Vec3::ZERO;
      }
    }
    let forward = transform.forward();
    let right = transform.right();
    transform.translation += options.velocity.x * dt * right
      + options.velocity.y * dt * Vec3::Y
      + options.velocity.z * dt * forward;

    if mouse_delta != Vec2::ZERO {
      // Apply look update
      let (pitch, yaw) = (
        (options.pitch - mouse_delta.y * 0.5 * options.sensitivity * dt).clamp(
          -0.99 * std::f32::consts::FRAC_PI_2,
          0.99 * std::f32::consts::FRAC_PI_2,
        ),
        options.yaw - mouse_delta.x * options.sensitivity * dt,
      );
      transform.rotation = Quat::from_euler(EulerRot::ZYX, 0.0, yaw, pitch);
      options.pitch = pitch;
      options.yaw = yaw;
    }
  }
}

fn generate_visible_chunks(
  mut commands: Commands,
  already_created_chunks: Query<&Chunk>,
  visible_chunks: Res<VisibleChunks>,
) {
  println!("Visible chunks: {:?}", visible_chunks);
  for visible_chunk_position in visible_chunks.chunks.iter() {
    if already_created_chunks
      .iter()
      .find(|c| c.position == *visible_chunk_position)
      .is_none()
    {
      commands
        .spawn()
        .insert(Chunk::new(visible_chunk_position.clone()).generate());
    }
  }
}

fn generate_mesh_for_visible_chunks(
  mut commands: Commands,
  chunks: Query<(Entity, &Chunk), Without<Handle<Mesh>>>,
  mut meshes: ResMut<Assets<Mesh>>,
) {
  for (entity, chunk) in chunks.iter() {
    let mesh = chunk.generate_mesh();
    commands.entity(entity).insert_bundle(PbrBundle {
      mesh: meshes.add(mesh),
      ..default()
    });
  }
}

fn highlight_player_chunk(
  mut commands: Commands,
  chunks: Query<(Entity, &GlobalTransform), (With<Chunk>, With<Handle<Mesh>>)>,
  camera: Query<&Transform, With<Camera>>,
) {
  let camera_position = camera.single();
  for (entity, transform) in chunks.iter() {
    println!("Camera: {:?}\n Chunk: {:?}", camera_position, transform);
  }
}

impl Plugin for VoxPlugin {
  fn build(&self, app: &mut bevy::prelude::App) {
    app
      .init_resource::<VisibleChunks>()
      .add_plugin(WireframePlugin)
      .add_startup_system(bootstrap)
      .add_system(generate_visible_chunks)
      .add_system(generate_mesh_for_visible_chunks)
      .add_system(highlight_player_chunk)
      .add_system(camera_controller);
  }
}
