use bevy::{
  math::Vec2,
  prelude::{default, Component, Mesh},
  render::mesh::{Indices, PrimitiveTopology},
};
use lazy_static::lazy_static;

type Voxel = bool;
type VertexPosition = [f32; 3];

pub const CHUNK_SIZE: usize = 16;
pub const CHUNK_HEIGHT: usize = 1;

#[derive(Default, Debug, Component)]
pub struct Chunk {
  pub position: Vec2,
  pub voxels: [[[Voxel; CHUNK_SIZE]; CHUNK_SIZE]; CHUNK_HEIGHT],
}

impl Chunk {
  pub fn new(position: Vec2) -> Self {
    Self {
      position,
      ..default()
    }
  }

  fn loop_through_mut<F>(&mut self, mut lambda: F)
  where
    F: FnMut(&mut bool, usize, usize, usize) -> (),
  {
    for y in 0..CHUNK_HEIGHT {
      for x in 0..CHUNK_SIZE {
        for z in 0..CHUNK_SIZE {
          lambda(&mut self.voxels[y][z][x], x, y, z);
        }
      }
    }
  }

  fn loop_through<F>(&self, mut lambda: F)
  where
    F: FnMut(&bool, usize, usize, usize) -> (),
  {
    for y in 0..CHUNK_HEIGHT {
      for x in 0..CHUNK_SIZE {
        for z in 0..CHUNK_SIZE {
          lambda(&self.voxels[y][z][x], x, y, z);
        }
      }
    }
  }

  pub fn generate(mut self) -> Self {
    self.loop_through_mut(|val, _, _, _| {
      *val = true;
    });
    self
  }

  pub fn generate_mesh(&self) -> Mesh {
    lazy_static! {
        static ref VOXEL_VERTICES: [VertexPosition; 8] = [
            [0.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
            [1.0, 1.0, 0.0],
            [0.0, 1.0, 0.0],
            [0.0, 0.0, 1.0],
            [1.0, 0.0, 1.0],
            [1.0, 1.0, 1.0],
            [0.0, 1.0, 1.0],
        ];

        static ref VOXEL_TRIANGLES: [[u32; 6]; 6] = [
            [ 0, 3, 1, 1, 3, 2],  // Back Face
            [ 5, 6, 4, 4, 6, 7],  // Front Face
            [ 3, 7, 2, 2, 7, 6],  // Top Face
            [ 1, 5, 0, 0, 5, 4],  // Bottom Face
            [ 4, 7, 0, 0, 7, 3],  // Left Face
            [ 1, 2, 5, 5, 2, 6],  // Right Face
        ];

        static ref VOXEL_FACE_OFFSETS: [[i32; 3]; 6] = [
            [0, 0, -1],
            [0, 0, 1],
            [0, 1, 0],
            [0, -1, 0],
            [-1, 0, 0],
            [1, 0, 0],
        ];

        static ref VOXEL_NORMALS: [[f32; 3]; 6] = [
            [0.0, 0.0, -1.0],
            [0.0, 0.0, 1.0],
            [0.0, 1.0, 0.0],
            [0.0, -1.0, 0.0],
            [-1.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
        ];

        static ref VOXEL_UVS: [[f32; 2]; 6] = [
            [ (211.0 / 255.0), 0.5 ],
            [ (210.0 / 255.0), 0.5 ],
            [ (219.0 / 255.0), 0.5 ],
            [ (213.0 / 255.0), 0.5 ],
            [ (210.0 / 255.0), 0.5 ],
            [ (211.0 / 255.0), 0.5 ],
        ];
    }

    let mut vertex_index = 0;
    let mut positions: Vec<[f32; 3]> = Vec::new();
    let mut normals: Vec<[f32; 3]> = Vec::new();
    let mut uvs: Vec<[f32; 2]> = Vec::new();
    let mut indices: Vec<u32> = Vec::new();

    self.loop_through(|value, x, y, z| {
      if !value {
        return;
      }

      for face_index in 0..6 {
        let x_offset = x as i32 + VOXEL_FACE_OFFSETS[face_index][0];
        let y_offset = y as i32 + VOXEL_FACE_OFFSETS[face_index][1];
        let z_offset = z as i32 + VOXEL_FACE_OFFSETS[face_index][2];

        let voxel_at_offset = if x_offset < 0
          || y_offset < 0
          || z_offset < 0
          || x_offset >= CHUNK_SIZE as i32
          || y_offset >= CHUNK_HEIGHT as i32
          || z_offset >= CHUNK_SIZE as i32
        {
          false
        } else {
          self.voxels[y_offset as usize][z_offset as usize][x_offset as usize]
        };

        if voxel_at_offset {
          continue;
        }

        for face_point_index in 0..6 {
          let index = VOXEL_TRIANGLES[face_index][face_point_index];
          let position = VOXEL_VERTICES[index as usize];
          positions.push([
            position[0] + x as f32 + self.position.x * CHUNK_SIZE as f32,
            position[1] + y as f32,
            position[2] + z as f32 + self.position.y * CHUNK_SIZE as f32,
          ]);
          uvs.push(VOXEL_UVS[face_index as usize]);
          normals.push(VOXEL_NORMALS[face_index as usize]);

          indices.push(vertex_index);
          vertex_index += 1;
        }
      }
    });

    let mut mesh = Mesh::new(PrimitiveTopology::TriangleList);
    mesh.set_indices(Some(Indices::U32(indices)));
    mesh.insert_attribute(Mesh::ATTRIBUTE_POSITION, positions);
    mesh.insert_attribute(Mesh::ATTRIBUTE_NORMAL, normals);
    mesh.insert_attribute(Mesh::ATTRIBUTE_UV_0, uvs);

    mesh
  }
}
