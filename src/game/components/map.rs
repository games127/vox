use bevy::prelude::Component;

pub const VISIBLE_RADIUS: usize = 1;
pub const QUAD_SIZE: usize = VISIBLE_RADIUS * 2 + 1;
pub const TOTAL_CHUNKS: usize = (VISIBLE_RADIUS * 2 + 1) * (VISIBLE_RADIUS * 2 + 1);

#[derive(Component)]
pub struct VisibleChunks {
  pub chunks: [[i32; 2]; TOTAL_CHUNKS],
}

impl Default for VisibleChunks {
  fn default() -> Self {
    let mut chunks: [[i32; 2]; TOTAL_CHUNKS] = Default::default();
    for x in 0..QUAD_SIZE {
      for y in 0..QUAD_SIZE {
        chunks[y * QUAD_SIZE + x] = [x as i32, y as i32];
      }
    }

    println!("Chunks: {:?}", chunks);
    Self { chunks }
  }
}
