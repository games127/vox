use bevy::prelude::*;
use game::VoxPlugin;

mod game;

fn main() {
  App::new()
    .add_plugins(DefaultPlugins)
    .add_plugin(VoxPlugin)
    .run();
}
